var Pez = require('../models/pez');

exports.pez_list = function(req, res){
    res.render('peces/index', {peces: Pez.allPeces});
}

exports.pez_create_get = function(req, res){
    res.render('peces/create');
}

exports.pez_create_post = function(req, res){
    var pez = new Pez(req.body.id, req.body.nombre, req.body.tipo);
    pez.ubicacion = [req.body.lat, req.body.lng];
    Pez.add(pez);

    res.redirect('/peces');
}

exports.pez_update_get = function(req, res){
    var pez = Pez.findById(req.params.id);

    res.render('peces/update', {pez});
}

exports.pez_update_post = function(req, res){

    var pez = Pez.findById(req.params.id);
    pez.id = req.body.id;
    pez.nombre = req.body.nombre;
    pez.tipo = req.body.tipo;
    pez.ubicacion = [req.body.lat, req.body.lng];
    
    res.redirect('/peces');
}

exports.pez_delete_post = function(req, res){
    Pez.removeById(req.body.id);

    res.redirect('/peces');
}