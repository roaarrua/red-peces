var Pez = function (id, nombre, tipo, ubicacion) {
    this.id = id;
    this.nombre = nombre;
    this.tipo = tipo;
    this.ubicacion = ubicacion;
}

Pez.prototype.toString = function (){
    return 'id: ' + this.id + ' | nombre: ' + this.nombre;
}

Pez.allPeces = [];
Pez.add = function(aPez){
    Pez.allPeces.push(aPez);
}

Pez.findById = function(aPezId){
    var aPez = Pez.allPeces.find(x => x.id == aPezId);
    if (aPez)
        return aPez;
    else
        throw new Error(`No existe un pez con el id ${aPezId}`);
}

Pez.removeById = function(aPezId){
    //FindById para agarrar el error en caso de que el id NO exista
    Pez.findById(aPezId);
    
    for(var i = 0; i < Pez.allPeces.length; i++){
        if(Pez.allPeces[i].id == aPezId){
            Pez.allPeces.splice(i, 1);
            break;
        }
    }
}

var a = new Pez(1, 'Goldfish', 'mar', [-34.628740, -58.347830]);
var b = new Pez(2, 'Betta', 'rio', [-34.245952, -58.726403]);

Pez.add(a);
Pez.add(b);

module.exports = Pez;