var express = require('express');
var router = express.Router();
var pezController = require('../controllers/pez');

router.get('/', pezController.pez_list);
router.get('/create', pezController.pez_create_get);
router.post('/create', pezController.pez_create_post);
router.get('/:id/update', pezController.pez_update_get);
router.post('/:id/update', pezController.pez_update_post);
router.post('/:id/delete', pezController.pez_delete_post);

module.exports = router;